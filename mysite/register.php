<html>

<head>
    <title>Log in</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>

    <div class="container">
        <div class="page-header">
            <div class="jumbotron">
                <h1>My Movies</h1>
            </div>
        </div>

        <div class="rows">
            <div class="col-xs-12 col-sm-12 col-lg-8">
                <div class="jumbotron">
                    <h1>Register</h1>

                    <form id='register' action='register.php' method='post' accept-charset='UTF-8'>
                        <fieldset>
                            <label for='name'>Your Full Name: </label>
                            <input type='text' name='name' id='name' maxlength="50" />
                            <label for='email'>Email Address*:</label>
                            <input type='text' name='email' id='email' maxlength="50" />

                            <label for='username'>UserName:</label>
                            <input type='text' name='username' id='username' maxlength="50" />

                            <label for='password'>Password:</label>
                            <input type='password' name='password' id='password' maxlength="50" />
                            <input type='submit' name='Submit' value='Submit' />

                        </fieldset>
                    </form>

                    <a href="hello.php">
                        <button class="btn btn-success">Volver!</button>
                    </a>

                </div>

            </div>

        </div>

    </div>
    <footer class="footer">
        <div class="container">
            <div class="well">
                <p class="text-muted">Made by Patryk Celiński and Carlos 2016</p>
            </div>
        </div>
    </footer>

</body>

</html>