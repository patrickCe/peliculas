<html>

<head>
    <title>Welcome!</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>

    <div class="container">
        <div class="page-header">
            <div class="jumbotron">
                <h1>My Movies</h1>
            </div>
        </div>

        <div class="rows">
            <div class="col-xs-12 col-sm-12 col-lg-8">
                <div class="jumbotron">
                    <h1>Welcome on our movie database!</h1>
                    <p>The best movie database on UPCT!</p>

                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                            <li data-target="#myCarousel" data-slide-to="3"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="movies1.jpg" alt="Movies1">
                            </div>

                            <div class="item">
                                <img src="movies2.jpg" alt="Movies2">
                            </div>

                            <div class="item">
                                <img src="movies3.jpg" alt="Movies3">
                            </div>

                            <div class="item">
                                <img src="movies4.jpg" alt="Movies4">
                            </div>
                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>

                </div>

            </div>
            <div class="col-lg-4">

                <div class="jumbotron">
                    <h3>FORM</h3>
<a href="register.php"><button class="btn btn-primary">Register</button></a>
<a href="login.php"><button class="btn btn-success">Log in</button></a>

                </div>

            </div>

        </div>

    </div>
    <footer class="footer">
        <div class="container">
            <div class="well">
                <p class="text-muted">Made by Patryk Celiński and Carlos 2016</p>
            </div>
        </div>
    </footer>

</body>

</html>
